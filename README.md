# Git Workflow for schema and data submission

This document describe a workflow for how the schema and the data submissions
can happen. Other large files by collectors should not be needed for everyone.

## Testable
This document is testable by running
[clitest](https://github.com/aureliojargas/clitest) with these arguments:
```sh
clitest --first README.md
```

## Continuous testing
```sh
echo README.md | entr clitest --first README.md
```

## Setup
We define three users, designer, collector1 and collector2 and make a function for both of them.

```console
$ export ROOT=/tmp/vcsformstest
$ designer () { export GIT_AUTHOR_NAME='Designer' GIT_AUTHOR_EMAIL='designer@example.com'; export GIT_COMMITTER_NAME="$GIT_AUTHOR_NAME"; export GIT_COMMITTER_EMAIL="$GIT_AUTHOR_EMAIL"; cd $ROOT/designer; }
$ collector1 () { export GIT_AUTHOR_NAME='Collector 1' GIT_AUTHOR_EMAIL='collector1@example.com'; export GIT_COMMITTER_NAME="$GIT_AUTHOR_NAME"; export GIT_COMMITTER_EMAIL="$GIT_AUTHOR_EMAIL"; cd $ROOT/collector1; }
$ collector2 () { export GIT_AUTHOR_NAME='Collector 2' GIT_AUTHOR_EMAIL='collector2@example.com'; export GIT_COMMITTER_NAME="$GIT_AUTHOR_NAME"; export GIT_COMMITTER_EMAIL="$GIT_AUTHOR_EMAIL"; cd $ROOT/collector2 ; }
$ rm -rf $ROOT && mkdir $ROOT
$ cd $ROOT
$ mkdir server designer collector1 collector2
$ export REMOTE=$ROOT/server
$
```

This makes it clear who is running the commands and gives stable output so that
the output can be diffed.

## Init a bare repo on the server that the designer can push to

```console
$ cd $ROOT/server
$ git init --bare -b main
Initialized empty Git repository in /tmp/vcsformstest/server/
$
```

## Designing a form

Normally a form is developed on a desktop by the designer.

Designer starts by creating the initial repository and including support for
git lfs. All files under folder assets are considered large:
```console
$ designer
$ git init -b main
Initialized empty Git repository in /tmp/vcsformstest/designer/.git/
$ git lfs install --local
Updated git hooks.
Git LFS initialized.
$ git lfs track '/schema/assets/**'
Tracking "/schema/assets/**"
$ git lfs track '/data/*/assets/**'
Tracking "/data/*/assets/**"
$ git add .gitattributes
$ git commit -m 'Init' #=> --exit 0
$
```

```console
$ mkdir schema
$ mkdir schema/assets
$ echo 'ScmForm { }' > schema/main.qml
$ echo 'BIGIMAGE' > schema/assets/manual.png
$ git add schema/main.qml
$ git add schema/assets/manual.png
$ git commit -m 'First version' #=> --exit 0
$ git remote add origin $REMOTE
$ git push --set-upstream origin main #=> --exit 0
$
```

Then Collector 1 and Collector 2 downloads the form, by cloning from server:
```console
$ collector1
$ git clone $REMOTE . #=> --exit 0
$
```

```console
$ collector2
$ git clone $REMOTE . #=> --exit 0
$ git lfs ls-files
6ca296db40 * schema/assets/manual.png
$ git ls-files
.gitattributes
schema/assets/manual.png
schema/main.qml
$
```

And when offline collects data including a big picture:
```console
$ collector1
$ mkdir -p data/uuid-1
$ mkdir data/uuid-1/assets
$ echo '{ "id": 1}' > data/uuid-1/data.json
$ echo 'BIGPHOTO1' > data/uuid-1/assets/photo.jpg
$ git add data/uuid-1
$ git commit -m 'Added uuid-1' #=> --egrep 2 files changed
$
```

About the same time Collector 2 was also collecting data and taking a picture:
```console
$ collector2
$ mkdir -p data/uuid-2
$ mkdir data/uuid-2/assets/
$ echo '{ "id": 2}' > data/uuid-2/data.json
$ echo 'BIGPHOTO2' > data/uuid-2/assets/photo.png
$ git add data/uuid-2
$ git commit -a -m 'Added uuid-2' #=> --egrep 2 files changed
$
```

When both are back online and ready to sync
```console
$ collector1
$ git pull origin main #=> --exit 0
$ git push origin main #=> --exit 0
$ git lfs ls-files
7438eb96c6 * data/uuid-1/assets/photo.jpg
6ca296db40 * schema/assets/manual.png
$
```

```console
$ collector2
$ git pull origin main #=> --exit 0
$ git push origin main #=> --exit 0
$ git lfs ls-files
7438eb96c6 * data/uuid-1/assets/photo.jpg
4ad6bf496d * data/uuid-2/assets/photo.png
6ca296db40 * schema/assets/manual.png
$
```

The designer can now pull down the data and analyze them:
```console
$ designer
$ git pull #=> --exit 0
$ git lfs ls-files
7438eb96c6 * data/uuid-1/assets/photo.jpg
4ad6bf496d * data/uuid-2/assets/photo.png
6ca296db40 * schema/assets/manual.png
$ git ls-files
.gitattributes
data/uuid-1/assets/photo.jpg
data/uuid-1/data.json
data/uuid-2/assets/photo.png
data/uuid-2/data.json
schema/assets/manual.png
schema/main.qml
$
```
